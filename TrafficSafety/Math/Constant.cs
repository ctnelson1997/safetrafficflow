﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSafety
{
   class Constant
   {
      #region Singleton Properties
      private static Constant _instance = new Constant();
      public static Constant instance
      {
         get
         {
            return _instance;
         }
      }

      private Constant() : this(DEFAULT_SAFE_AADT, DEFAULT_SAFE_SPEED)
      { /* ¯\_(ツ)_/¯ */ }

      private Constant(int safeAadt, int safeSpeed)
      {
         this.SAFE_AADT = safeAadt;
         this.SAFE_SPEED = safeSpeed;
      }
      #endregion

      private const int DEFAULT_SAFE_AADT  = 1300;
      private const int DEFAULT_SAFE_SPEED = 10;

      private const int ONE_LANE_WIDTH = 100;
      private const int TWO_LANE_WIDTH = 150;

      public readonly int SAFE_AADT;
      public readonly int SAFE_SPEED;

      public static void Reset(int safeAadt, int safeSpeed)
      {
         _instance = new Constant(safeAadt, safeSpeed);
      }

      public int DetermineWidth(int lanes)
      {
         switch(lanes)
         {
            case 1: return ONE_LANE_WIDTH;
            case 2: return TWO_LANE_WIDTH;
            default: return int.MaxValue;
         }
      }
   }
}
