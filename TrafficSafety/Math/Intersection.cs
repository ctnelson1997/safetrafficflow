﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSafety.Objects
{
   class Intersection
   {
      public Pathway[] pathways { get; private set; }
      public int width { get; private set; }

      private const string TRAFFIC_LIGHT = "Traffic Light";
      private const string STOP_SIGN = "Stop Sign";
      private const string ROUNDABOUT = "Roundabout";

      public Intersection(Pathway[] pathways, int width)
      {
         this.pathways = pathways;
         this.width = width;
      }

      public string DetermineControl()
      {
         int p_aadt = this.GetPathwayLowestAADT().aadt;
         int p_speed = this.GetPathwayHighestSpeed().speed;
         int p_lanes = this.GetPathwayMostLanes().lanes;
         if (p_aadt > Constant.instance.SAFE_AADT && p_speed > Constant.instance.SAFE_SPEED)
            if (this.width < Constant.instance.DetermineWidth(p_lanes))
               return TRAFFIC_LIGHT;
            else
               return ROUNDABOUT;
         else
            return STOP_SIGN;
      }

      private Pathway GetPathwayLowestAADT()
      {
         int mindex = 0;
         for (int i = 1; i < pathways.Length; i++)
            if (pathways[mindex].aadt > pathways[i].aadt)
               mindex = i;
         return pathways[mindex];
      }

      private Pathway GetPathwayHighestSpeed()
      {
         int maxdex = 0;
         for (int i = 1; i < pathways.Length; i++)
            if (pathways[maxdex].speed < pathways[i].speed)
               maxdex = i;
         return pathways[maxdex];
      }

      private Pathway GetPathwayMostLanes()
      {
         int maxdex = 0;
         for (int i = 1; i < pathways.Length; i++)
            if (pathways[maxdex].lanes < pathways[i].lanes)
               maxdex = i;
         return pathways[maxdex];
      }

   }
}
