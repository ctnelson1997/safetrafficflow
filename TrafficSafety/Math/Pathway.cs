﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSafety.Objects
{
   class Pathway
   {
      public int aadt { get; private set; }
      public int speed { get; private set; }
      public int lanes { get; private set; }

      public Pathway(int aadt, int speed, int lanes)
      {
         this.aadt = aadt;
         this.speed = speed;
         this.lanes = lanes;
      }
   }
}
