﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSafety.Objects
{
   class Output
   {
      #region Singleton Properties
      private static Output _instance = new Output();
      public static Output instance
      {
         get
         {
            return _instance;
         }
      }

      private Output() { /* ¯\_(ツ)_/¯ */ }
      #endregion

      public void Run()
      {
         this.PrintMainHeader();
         this.DefineConstants();
         while (true)
            this.DoMath();
      }

      private void DefineConstants()
      {
         this.PrintSetupHeader();
         string response = Console.ReadLine();
         if (response.ToUpper().Equals("N"))
            return;
         Console.WriteLine("Define the safe aadt: ");
         int respAadt = int.Parse(Console.ReadLine());
         Console.WriteLine("Define the safe speed: ");
         int respSpeed = int.Parse(Console.ReadLine());
         Constant.Reset(respAadt, respSpeed);
         this.DefineConstants();
      }

      private void DoMath()
      {
         this.PrintMathHeader();
         Console.WriteLine("(gamma) What is the width of the intersection (default: feet)?");
         int widthIntersection = int.Parse(Console.ReadLine());
         Console.WriteLine();
         Console.WriteLine("(|rho|) How many pathways connect to the road?");
         int numPathways = int.Parse(Console.ReadLine());
         Console.WriteLine();
         Pathway[] pathways = new Pathway[numPathways];
         Console.ForegroundColor = ConsoleColor.Gray;
         for (int i = 1; i <= numPathways; i++)
         {
            Console.WriteLine("Pathway #" + i);
            Console.WriteLine("(tau) What is AADT of the pathway?");
            int pathwayAadt = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("(kappa) What is the speed of the pathway?");
            int pathwaySpeed = int.Parse(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("(epsilon) How many lanes are on the pathway?");
            int pathwayLanes = int.Parse(Console.ReadLine());
            Console.WriteLine();
            pathways[i - 1] = new Pathway(pathwayAadt, pathwaySpeed, pathwayLanes);
         }
         Intersection intersection = new Intersection(pathways, widthIntersection);
         Console.ForegroundColor = ConsoleColor.Yellow;
         Console.WriteLine("For this intersection, we recommend that you install...");
         Console.WriteLine(intersection.DetermineControl());
         Console.WriteLine();
      }

      private void PrintMainHeader()
      {
         Console.ForegroundColor = ConsoleColor.Cyan;
         Console.WriteLine(" -----------------------------------------");
         Console.WriteLine("   Safety of Traffic Flow                 ");
         Console.WriteLine("       A Focus on Intersection Selection  ");
         Console.WriteLine(" -----------------------------------------");
         Console.WriteLine("   C. Nelson   J. Werkheiser   D. Zinnel  ");
      }

      public void PrintSetupHeader()
      {
         Console.WriteLine();
         Console.WriteLine("The following constants have been defined");
         Console.WriteLine(" - SAFE AADT = " + Constant.instance.SAFE_AADT);
         Console.WriteLine(" - SAFE SPEED = " + Constant.instance.SAFE_SPEED);
         Console.WriteLine("Do you wish to change these (Y/N)?");
      }

      private void PrintMathHeader()
      {
         Console.ForegroundColor = ConsoleColor.Magenta;
         Console.WriteLine();
         Console.WriteLine("Now we can calculate what type of traffic control to use.");
         Console.WriteLine("Based on your inputs, we will suggest using either");
         Console.WriteLine("a traffic light, a stop sign, or a roundabout.");
         Console.WriteLine();
      }
   }
}
